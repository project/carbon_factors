<?php

namespace Drupal\carbon_factors\Plugin\Field\FieldType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TraversableTypedDataInterface;

/**
 * @FieldType(
 *   id = "carbon_factors_computed",
 *   label = @Translation("Carbon Factors"),
 *   description = @Translation("Compute and display Carbon Factors on an Entity."),
 *   category = @Translation("General"),
 *   default_widget = "carbon_factors_computed",
 *   default_formatter = "carbon_factors_computed",
 *   list_class = "\Drupal\carbon_factors\Field\CarbonFactorsFieldItemList",
 * )
 */
class CarbonFactorsItem extends FieldItemBase {

  /**
   * @var \Drupal\core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\carbon_factors\CarbonFactorsTypeManager
   */
  protected $carbonFactorsTypeManager;

  /**
   * {@inheritDoc}
   */
  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    /** @var self $instance */
    $instance = parent::createInstance($definition, $name, $parent);
    $instance->entityFieldManager = \Drupal::service('entity_field.manager');
    $instance->carbonFactorsTypeManager = \Drupal::service('plugin.manager.carbon_factors_type');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * We don't do any SQL storage, but this is still required until https://www.drupal.org/project/drupal/issues/2932273
   * is in to avoid exceptions being thrown.
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings() {
    return [
      'date_field' => '',
      'carbon_factors_type_filter' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $entityTypeID = $this->getEntity()->getEntityTypeId();
    $bundle = $this->getEntity()->bundle();
    $fields = $this->entityFieldManager->getFieldDefinitions($entityTypeID, $bundle);

    $date_fields = [];
    foreach ($fields as $field) {
      if ($field->getType() == 'created' || $field->getType() == 'changed' || $field->getType() == 'datetime') {
        $date_fields[$field->getName()] = $field->getName();
      }
    }

    $element['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Date field'),
      '#options' => $date_fields,
      '#empty_value' => '',
      '#default_value' => isset($settings['date_field']) ? $settings['date_field'] : NULL,
      '#required' => TRUE,
      '#description' => $this->t('Specify which field contains the date that will be used to determine which Carbon Factors this field should supply.'),
    ];

    $carbon_factors_type_plugin_definitions = $this->carbonFactorsTypeManager->getDefinitions();
    $carbon_factors_type_plugin_options = [];
    foreach ($carbon_factors_type_plugin_definitions as $id=>$plugin) {
      $label = $plugin['label'] instanceof TranslatableMarkup ? $plugin['label']->getUntranslatedString() : $plugin['label'];
      $carbon_factors_type_plugin_options[$id] = $label;
    }

    $element['carbon_factors_type_filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Carbon factors Type to filter by'),
      '#options' => $carbon_factors_type_plugin_options,
      '#empty_value' => '',
      '#default_value' => isset($settings['carbon_factors_type_filter']) ? $settings['carbon_factors_type_filter'] : NULL,
      '#description' => $this->t('When this field calculates the most relevant Carbon Factors to supply, it will most likely be desirable to only consider Carbon Factors which are of a particular type, this option lets you do that by selecting which Carbon Factors Plugin should be used.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * We don't do any SQL storage, but this is still required until https://www.drupal.org/project/drupal/issues/2932273
   * is in to avoid exceptions being thrown.
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Value'));

    return $properties;
  }

  /**
   * Because data for this field is never actually saved to the database, we need to tell Drupal that
   * this field is not empty, otherwise the formatter won't be displayed and it won't show on the
   * canonical view.
   *
   * {@inheritDoc}
   */
  public function isEmpty() {
    return FALSE;
  }

}
