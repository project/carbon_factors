<?php

namespace Drupal\carbon_factors\Plugin\Field\FieldWidget;

use Drupal\carbon_factors\Field\CarbonFactorsFieldItemListInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "carbon_factors_computed",
 *   label = @Translation("Carbon Factors"),
 *   field_types = {
 *     "carbon_factors_computed"
 *   }
 * )
 */
class CarbonFactorsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $output = '';
    if ($items instanceof CarbonFactorsFieldItemListInterface) {
      $carbon_factors_entity = $items->getMostRelevantCarbonFactorsEntity();
      if ($carbon_factors_entity != NULL) {
        $output = $carbon_factors_entity->label();
      }
      else {
        $output = '<em>No applicable Carbon Factors found.</em>';
      }
    }

    $element['value'] = [
      '#markup' => $this->t('<strong>%label:</strong> %output', [
        '%label' => $items->getFieldDefinition()->getLabel(),
        '%output' => $output,
      ]),
    ];

    return $element;
  }

}
