<?php

namespace Drupal\carbon_factors\Plugin\Field\FieldFormatter;

use Drupal\carbon_factors\Field\CarbonFactorsFieldItemListInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * @FieldFormatter(
 *   id = "carbon_factors_computed",
 *   label = @Translation("SCF Computed Field"),
 *   field_types = {
 *     "carbon_factors_computed"
 *   }
 * )
 */
class CarbonFactorsFieldFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $output = '';
    if ($items instanceof CarbonFactorsFieldItemListInterface) {
      $carbon_factors_entity = $items->getMostRelevantCarbonFactorsEntity();
      if ($carbon_factors_entity != NULL) {
        $output = $carbon_factors_entity->label();
      }
      else {
        $output = '<em>No applicable Carbon Factors found.</em>';
      }
    }

    $element['value'] = [
      '#markup' => $output,
    ];

    return $element;
  }

}
