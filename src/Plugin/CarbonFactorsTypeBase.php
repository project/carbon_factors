<?php

namespace Drupal\carbon_factors\Plugin;

use Drupal\carbon_factors\CarbonFactorsTypeInterface;
use Drupal\Core\Plugin\PluginBase;

abstract class CarbonFactorsTypeBase extends PluginBase implements CarbonFactorsTypeInterface {

  public $entity;

  /**
   * {@inheritDoc}
   */
  public function getEntity() {
    return $this->entity;
  }

}