<?php

namespace Drupal\carbon_factors;

use Drupal\Core\Datetime\DrupalDateTime;

interface CarbonFactorsServiceInterface {

  /**
   * Given a specific date and optionally the ID of a Carbon Factors Type Plugin, a array of Carbon Factors Entities
   * will be returned where the date provided will be within each Carbon Factors Entity's start_date and end_date.
   *
   * Carbon Factors Entities where the start and/or end date is not specified are considered to always apply and
   * therefor will always be returned.
   *
   * The returned array will be sorted in descending order by start date, keyed by the Entity ID, therefor the first
   * item should always be the most recent Carbon Factors Entity.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   DrupalDateTime object to use when querying for Carbon Factors Entities.
   * @param string|NULL $carbon_factors_type
   *   [optional] Specify the ID of a Carbon Factors Plugin Type to filter results by.
   *
   * @return \Drupal\carbon_factors\Entity\CarbonFactorsInterface[]
   *   An array of matching Carbon Factors Entities, keyed by Entity ID, with the most recent start date first, or an
   *   empty array if no matching entities could be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCarbonFactorsForDate(DrupalDateTime $date, $carbon_factors_type = NULL);

}