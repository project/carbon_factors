<?php

namespace Drupal\carbon_factors\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\carbon_factors\Entity\CarbonFactors;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CarbonFactorsForm extends EntityForm {

  /**
   * @var \Drupal\carbon_factors\CaronFactorsTypeManagerInterface
   */
  protected $carbonFactorsTypeManager;

  public static function create(ContainerInterface $container) {
    /** @var self $instance */
    $instance = parent::create($container);
    $instance->carbonFactorsTypeManager = $container->get('plugin.manager.carbon_factors_type');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\carbon_factors\Entity\CarbonFactorsInterface $entity */
    $entity = $this->entity;

    $form['#prefix'] = '<div id="carbon_factor_form_ajax_wrapper">';
    $form['#suffix'] = '</div>';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Label for this carbon factor."),
      '#required' => TRUE,
      '#weight' => '-40'
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => CarbonFactors::class . '::load',
      ],
      '#disabled' => !$entity->isNew(),
      '#weight' => '-40'
    ];

    $form['start_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('The date from which these Carbon Factors start.'),
      '#default_value' => $entity->getStartDate(),
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#weight' => '-30'
    ];

    $form['end_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End date'),
      '#description' => $this->t('The date from which these Carbon Factors end.'),
      '#default_value' => $entity->getEndDate(),
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#weight' => '-30'
    ];

    $carbon_factors_type_plugin_definitions = $this->carbonFactorsTypeManager->getDefinitions();
    $carbon_factors_type_plugin_options = [];
    foreach ($carbon_factors_type_plugin_definitions as $id=>$plugin) {
      $label = $plugin['label'] instanceof TranslatableMarkup ? $plugin['label']->getUntranslatedString() : $plugin['label'];
      $carbon_factors_type_plugin_options[$id] = $label;
    }

    $carbon_factors_type_plugin_id = $entity->getCarbonFactorsTypeID();

    $form['carbon_factors_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Carbon Factors Type Plugin'),
      '#description' => $this->t('Select which plugin to use to provide the Carbon Factors.'),
      '#default_value' => $carbon_factors_type_plugin_id,
      '#options' => $carbon_factors_type_plugin_options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxUpdateCarbonFactorTypePluginForm',
        'disable-refocus' => TRUE,
        'wrapper' => 'carbon_factor_form_ajax_wrapper',
      ],
      '#weight' => '-25'
    ];

    $carbon_factors_type_plugin_id = $form_state->hasValue('carbon_factors_type') ? $form_state->getValue('carbon_factors_type') : $carbon_factors_type_plugin_id;
    if (isset($carbon_factors_type_plugin_id)) {
      /** @var \Drupal\carbon_factors\Plugin\CarbonFactorsTypeBase $carbon_factors_type_plugin */
      $carbon_factors_type_plugin = $this->carbonFactorsTypeManager->createInstance($carbon_factors_type_plugin_id);
      $carbon_factors_type_plugin->entity = $this->getEntity();
      $form = $carbon_factors_type_plugin->form($form, $form_state);
    }

    return $form;
  }

  public function ajaxUpdateCarbonFactorTypePluginForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\carbon_factors\Entity\CarbonFactorsInterface $entity */
    $entity = $this->entity;

    $start_date = $form_state->getValue('start_date');
    if ($start_date instanceof DrupalDateTime) {
      $entity->setStartDate($start_date);
    }

    $end_date = $form_state->getValue('end_date');
    if ($end_date instanceof DrupalDateTime) {
      $entity->setEndDate($end_date);
    }

    if ($form_state->hasValue('carbon_factors_type')) {
      /** @var \Drupal\carbon_factors\Plugin\CarbonFactorsTypeBase $carbon_factors_type_plugin */
      $carbon_factors_type_plugin = $this->carbonFactorsTypeManager->createInstance($form_state->getValue('carbon_factors_type'));
      $carbon_factors_type_plugin->entity = $this->getEntity();
      $carbon_factors_type_plugin->save($form, $form_state);
    }

    $status = $entity->save();

    if ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('<em>%label</em> has been successfully created.', [
        '%label' => $entity->label(),
      ]));
    } else {
      $this->messenger()->addMessage($this->t('<em>%label</em> has been successfully updated.', [
        '%label' => $entity->label(),
      ]));
    }
    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

}
