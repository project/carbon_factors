<?php

namespace Drupal\carbon_factors;

use Drupal\Core\Form\FormStateInterface;

interface CarbonFactorsTypeInterface {

  /**
   * Gets the Carbon Factor Config Entity which this plugin instance has been used on.
   *
   * @return \Drupal\carbon_factors\Entity\CarbonFactorsInterface
   */
  public function getEntity();

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function form($form, FormStateInterface $form_state);

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function save($form, FormStateInterface $form_state);

}