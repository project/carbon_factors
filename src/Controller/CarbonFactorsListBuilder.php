<?php

namespace Drupal\carbon_factors\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;

class CarbonFactorsListBuilder extends ConfigEntityListBuilder {

  /**
   * Remove the had coded sort.
   *
   * {@inheritDoc}
   */
  public function load() {
    $entity_ids = $this->getEntityIds();
    $entities = $this->storage->loadMultipleOverrideFree($entity_ids);
    return $entities;
  }

  /**
   * Add the tableSort to the Entity Query.
   *
   * {@inheritDoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->tableSort($this->buildHeader());

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
        'label' => [
          'data' => $this->t('Carbon factors label'),
          'field' => 'label',
          'specifier' => 'label'
        ],
        'id' => [
          'data' => $this->t('Machine name'),
          'field' => 'id',
          'specifier' => 'id'
        ],
        'start_date' => [
          'data' => $this->t('Start date'),
          'field' => 'start_date',
          'specifier' => 'start_date'
        ],
        'end_date' => [
          'data' => $this->t('End date'),
          'field' => 'end_date',
          'specifier' => 'end_date'
        ]
      ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\carbon_factors\Entity\CarbonFactorsInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $start_date = $entity->getStartDate();
    $row['start_date'] = $start_date instanceof DrupalDateTime ? $start_date->format('Y-m-d') : '';
    $end_date = $entity->getEndDate();
    $row['end_date'] = $end_date instanceof DrupalDateTime ? $end_date->format('Y-m-d') : '';
    return $row + parent::buildRow($entity);
  }

}
