<?php

namespace Drupal\carbon_factors\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Interface CarbonFactorInterface
 */
interface CarbonFactorsInterface extends ConfigEntityInterface {

  /**
   * Gets the date which these Carbon Factors started.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|NULL
   *   DrupalDateTime object containing the date or null if not defined.
   */
  public function getStartDate();

  /**
   * Sets the date which these Carbon Factors start on.
   *
   * @param DrupalDateTime $date
   *   DrupalDateTime object containing the date or null if no start date should be defined.
   *
   * @return self
   */
  public function setStartDate(DrupalDateTime $date);

  /**
   * Gets the date which these Carbon Factors ended.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|NULL
   *   DrupalDateTime object containing the date or null if not defined.
   */
  public function getEndDate();

  /**
   * Sets the date which these Carbon Factors start on.
   *
   * @param DrupalDateTime $date
   *   DrupalDateTime object containing the date or null if no start date should be defined.
   *
   * @return self
   */
  public function setEndDate(DrupalDateTime $date);

  /**
   * Gets the Carbon Factors Type Plugin ID which is in use.
   *
   * @return string
   *   Carbon Factors Type Plugin ID.
   */
  public function getCarbonFactorsTypeID();

  /**
   * Gets the Carbon Factors Type Plugin Class which is in use.
   *
   * @return \Drupal\carbon_factors\CarbonFactorsTypeInterface
   *   Carbon Factors Type class object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function getCarbonFactorsType();

}
