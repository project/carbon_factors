<?php

namespace Drupal\carbon_factors\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * @ConfigEntityType(
 *   id = "carbon_factors",
 *   label = @Translation("Carbon factors"),
 *   label_collection = @Translation("Carbon factors"),
 *   handlers = {
 *     "list_builder" = "Drupal\carbon_factors\Controller\CarbonFactorsListBuilder",
 *     "form" = {
 *       "default" = "Drupal\carbon_factors\Form\CarbonFactorsForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "access" = "Drupal\carbon_factors\Access\CarbonFactorsAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "carbon_factors",
 *   admin_permission = "administer carbon_factors entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/carbon_factors/add",
 *     "edit-form" = "/admin/structure/carbon_factors/{carbon_factors}/edit",
 *     "delete-form" = "/admin/structure/carbon_factors/{carbon_factors}/delete",
 *     "collection" = "/admin/structure/carbon_factors"
 *   }
 * )
 */
class CarbonFactors extends ConfigEntityBase implements CarbonFactorsInterface {

  /**
   * @var \Drupal\carbon_factors\CaronFactorsTypeManagerInterface
   */
  protected $carbonFactorsTypeManager;

  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->carbonFactorsTypeManager = \Drupal::service('plugin.manager.carbon_factors_type');
  }

  /**
   * The Application type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Application type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $start_date;

  /**
   * {@inheritDoc}
   */
  public function getStartDate() {
    $date = $this->start_date;
    if (is_array($date) && (isset($date['object']) || $date['object'] == NULL)) {
      return $date['object'];
    }
    return isset($date) ? new DrupalDateTime($date) : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setStartDate(DrupalDateTime $date) {
    $this->start_date = $date instanceof DrupalDateTime ? $date->format('Y-m-d') : NULL;
    return $this;
  }

  /**
   * @var string
   */
  protected $end_date;

  /**
   * {@inheritDoc}
   */
  public function getEndDate() {
    $date = $this->end_date;
    if (is_array($date) && (isset($date['object']) || $date['object'] == NULL)) {
      return $date['object'];
    }
    return isset($date) ? new DrupalDateTime($date) : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setEndDate(DrupalDateTime $date) {
    $this->end_date = $date instanceof DrupalDateTime ? $date->format('Y-m-d') : NULL;
    return $this;
  }

  /**
   * @var string
   */
  protected $carbon_factors_type;

  /**
   * {@inheritDoc}
   */
  public function getCarbonFactorsTypeID() {
    return $this->carbon_factors_type;
  }

  /**
   * {@inheritDoc}
   */
  public function getCarbonFactorsType() {
    /** @var \Drupal\carbon_factors\Plugin\CarbonFactorsTypeBase $instance */
    $instance = $this->carbonFactorsTypeManager->createInstance($this->carbon_factors_type);
    $instance->entity = $this;
    return $instance;
  }

}
