<?php

namespace Drupal\carbon_factors;

use Drupal\carbon_factors\Entity\CarbonFactorsInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

class CarbonFactorsService implements CarbonFactorsServiceInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function getCarbonFactorsForDate(DrupalDateTime $date, $carbon_factors_type = NULL) {
    $date_formatted = $date->format('Y-m-d');
    $storage = $this->entityTypeManager->getStorage('carbon_factors');
    $query = $storage->getQuery();

    if ($carbon_factors_type != NULL) {
      $query->condition('carbon_factors_type', $carbon_factors_type, '=');
    }

    $query->condition($query->orConditionGroup()
      ->condition($query->andConditionGroup()
        ->condition('start_date', $date_formatted, '<=')
        ->condition('end_date', $date_formatted, '>=')
      )
      ->condition($query->andConditionGroup()
        ->notExists('start_date')
        ->notExists('end_date')
      )
      ->condition($query->andConditionGroup()
        ->condition('start_date', $date_formatted, '<=')
        ->notExists('end_date')
      )
      ->condition($query->andConditionGroup()
        ->condition('end_date', $date_formatted, '>=')
        ->notExists('start_date')
      )
    );

    $query->sort('start_date', 'DESC');

    $entities = $query->execute();
    return $storage->loadMultiple($entities);
  }

}