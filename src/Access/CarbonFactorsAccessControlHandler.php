<?php

namespace Drupal\carbon_factors\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class CarbonFactorsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritDoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'view label') {
      return AccessResult::allowed(); // todo: determine if this needs a permission
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}