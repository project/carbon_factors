<?php

namespace Drupal\carbon_factors\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class CarbonFactorsType
 *
 * Defines a Carbon Factors Type object.
 *
 * @package Drupal\carbon_factors\Annotation
 *
 * @Annotation
 */
class CarbonFactorsType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Carbon Factors Type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The name of the Carbon Factors Type class.
   *
   * This is not provided manually, it will be added by the discovery mechanism.
   *
   * @var string
   */
  public $class;

}