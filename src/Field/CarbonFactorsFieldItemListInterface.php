<?php

namespace Drupal\carbon_factors\Field;

use Drupal\Core\Field\FieldItemListInterface;

interface CarbonFactorsFieldItemListInterface extends FieldItemListInterface {

  /**
   * This is a wrapper method for CarbonFactorsServiceInterface::getCarbonFactorsForDate() but within the context of
   * this field.
   *
   * Returns a sub-set of Carbon Factors Entities filtered based on the contexts and constraints set within the field
   * settings.
   *
   * For example in the field settings it is possible to filter Carbon Factors Entities by a specific Carbon Factors
   * Type Plugin, so with that specified only Carbon Factors Entities which use that Type Plugin will be returned.
   * Similarly in the field settings a date field can be specified, so the date set in that field will be used to
   * further filter the returned set of Entities to only ones where the start_date and end_date of the Carbon Factors
   * Entities are relevant.
   *
   * @see \Drupal\carbon_factors\CarbonFactorsServiceInterface::getCarbonFactorsForDate()
   *   The underlying method which the constraints are passed to and is used to query the Entity Storage for the Carbon
   *   Factors Entities.
   *
   * @return \Drupal\carbon_factors\Entity\CarbonFactorsInterface[]
   *   An array of matching Carbon Factors Entities, keyed by Entity ID, with the most recent start date first, or an
   *   empty array if no matching entities could be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getApplicableCarbonFactorsEntities();

  /**
   * This is a wrapper method for self::getApplicableCarbonFactorsEntities(), this method takes the array returned by
   * that function and of the items in the array returns the single most relevant Carbon Factors Entity.  In practice
   * this is usually the Entity at array index 0, the array returned by getApplicableCarbonFactorsEntities() is already
   * sorted with the first item being the most relevant.
   *
   * @see self::getApplicableCarbonFactorsEntities()
   *
   * @see \Drupal\carbon_factors\CarbonFactorsServiceInterface::getCarbonFactorsForDate()
   *   For more details on how the underlying array of Entities is sorted and so what is determined to be the "most
   *   relevant" Carbon Factors Entity.
   *
   * @return \Drupal\carbon_factors\Entity\CarbonFactorsInterface|NULL
   *   An array of Carbon Factors Entities which are most relevant, or an empty array if none could be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getMostRelevantCarbonFactorsEntity();

}