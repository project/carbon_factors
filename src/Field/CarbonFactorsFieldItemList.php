<?php

namespace Drupal\carbon_factors\Field;

use Drupal\carbon_factors\Entity\CarbonFactorsInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\TraversableTypedDataInterface;

class CarbonFactorsFieldItemList extends FieldItemList implements CarbonFactorsFieldItemListInterface {

  /**
   * @var \Drupal\carbon_factors\CarbonFactorsServiceInterface
   */
  protected $carbonFactorsService;

  /**
   * @var \Drupal\carbon_factors\Entity\CarbonFactorsInterface[]|NULL
   */
  private $carbonFactorsEntities = NULL;

  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    /** @var self $instance */
    $instance = parent::createInstance($definition, $name, $parent);
    $instance->carbonFactorsService = \Drupal::service('carbon_factors');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getApplicableCarbonFactorsEntities() {
    if ($this->carbonFactorsEntities != NULL) {
      // We use this as a temporary cache, so that if there are multiple calls to this function in the same request stack
      // we only have to run all of the logic below the first time.
      return $this->carbonFactorsEntities;
    }

    $entity = $this->getEntity();
    $field_name = $this->getSetting('date_field');
    if (!$entity->hasField($field_name)) {
      $this->carbonFactorsEntities = [];
      return $this->carbonFactorsEntities;
    }
    $field_list = $entity->get($field_name);
    if ($field_list->count() <= 0) {
      $this->carbonFactorsEntities = [];
      return $this->carbonFactorsEntities;
    }

    $field_value = $field_list->get(0)->getValue()['value'];
    $field_type = $field_list->getFieldDefinition()->getType();
    if ($field_type == 'created' || $field_type == 'changed') {
      $date = DrupalDateTime::createFromTimestamp($field_value);
    }
    elseif ($field_type == 'datetime') {
      $date = new DrupalDateTime($field_value);
    }
    else {
      $this->carbonFactorsEntities = [];
      return $this->carbonFactorsEntities;
    }

    $carbon_factors_type_filter = $this->getSetting('carbon_factors_type_filter');
    $this->carbonFactorsEntities = $this->carbonFactorsService->getCarbonFactorsForDate($date, $carbon_factors_type_filter != '' ? $carbon_factors_type_filter : NULL);

    return $this->carbonFactorsEntities;
  }

  /**
   * {@inheritDoc}
   */
  public function getMostRelevantCarbonFactorsEntity() {
    $entities = $this->getApplicableCarbonFactorsEntities();
    // Get the first item in the array, as the arrays returned by EntityQuery are keyed by Entity ID, so we can't assume
    // $entities[0] exits.
    $first = reset($entities);
    return $first instanceof CarbonFactorsInterface ? $first : NULL;
  }


}